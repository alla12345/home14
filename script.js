//1------------
let arr = [1, 3, 23, "mama", { a: 1, b: "2" }];

//2-----------
// const arrObj = arr.slice(4,5);
const arrObj = arr[arr.length-1];
console.log('arr задание 2', arr);
console.log('arrObj', arrObj);

//3------
const x = arr[0]; 
arr[0] = arr[1];
arr[1] = x;
console.log('arr задание 3', arr);

//4----------
const arrTwo = ['ek', 82];
arr = [...arr, ...arrTwo];
console.log('arr задание 4', arr);

//5--------------
for (key in arr){
    if (typeof arr[key] === 'number') {
      arr[key] = arr[key] + 2;   
    }
}
console.log('arr задание 5', arr);

//6--------------
const arrNumbers = [2, 56, 23, 1, 2, 1];
console.log('arr задание 6', arrNumbers);

//7--------------
let minArr = arrNumbers[0];
for (key in arrNumbers) {
  if (minArr > arrNumbers[key]) {
    minArr = arrNumbers[key];
  }
}

console.log("minArr задание 7", minArr);
console.log("индекс задание 7", arrNumbers.indexOf(minArr));
const index =  arrNumbers.indexOf(minArr);
arrNumbers[index] = 99;
console.log("minArr задание 7", arrNumbers);

//9----------------------------------------------------------
//console.log("Задание 9", arrNumbers);
//max
let maxArr = arrNumbers[0];
for (key in arrNumbers) {
  if (maxArr < arrNumbers[key]) {
    maxArr = arrNumbers[key];
  }
}
const indmax =  arrNumbers.indexOf(maxArr);//1-й из max
console.log("maxArr  задание 9", maxArr);
console.log("indmax 1-й задание 9", indmax);

//min
let minAr = arrNumbers[0];
for (key in arrNumbers) {
  if (minAr > arrNumbers[key]) {
    minAr = arrNumbers[key];
  }
}
const indmin =  arrNumbers.indexOf(minAr);
console.log("minArr задание 9", minAr);
console.log("indmin задание 9", indmin);

// мб развернуть массив
const revArrNumbers =[...arrNumbers];
revArrNumbers.reverse();
console.log("revArrNumbers", revArrNumbers);
console.log("arrNumbers", arrNumbers);
const indmaxr =  revArrNumbers.indexOf(maxArr);//1-й из max реверс
console.log('indmaxr', indmaxr);
console.log("длина", arrNumbers.length);
const imax = (arrNumbers.length - 1) - indmaxr;
console.log("индекс max последний imax", imax);
//-------
const newArrNumbers = arrNumbers.slice(indmin, imax);
//const newArrNumbers = arrNumbers.slice(3, 5); - это просто проверка
console.log("newArr задание 9", newArrNumbers);

//------------------------------------------------------------

