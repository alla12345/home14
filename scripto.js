//1------------
const a = {
    b : 2,
    c : 10,
    z : 7,
};
let min = a.b;
let minKey = 'b';
for (key in a) {
  if (min > a[key]) {
    min = a[key];
    minKey = key;
  }
}
console.log('minKey', minKey);
delete a[minKey]; // это мне подсказали [minKey]  
console.log('a', a);
console.log('min', min);

//2-------------
//вариант 1
// const b = {...a};
// b.f = {
//     h: 'hi',
//     j: 123, 
// }
//вариант 2
const b = {...a, f:{h: 'hi', j : 123}};
console.log('b', b);

//3-------------
b.f.j = min;
console.log(b);
console.log('j', b.f.j);

//4-----------
// if ('b' in a) {
//     console.log('да');
// } else {
//     console.log('нет');
// }
console.log("задание 4");
(a.b) ?  console.log('да') : console.log('нет');

//5-----------
const aa = {b: 5};
const hh = aa;
hh.b = 78;
console.log('hh', hh);
console.log('aa', aa); 
